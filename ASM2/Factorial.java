public class Factorial {
	public static long calculate (int n) {
		if (n == 0) return 1;
		return (n * calculate(n-1));
	}
	
	public static void checkIfPassed(int n, long rs) {
		if (n==0) {
			if (rs == 1) 
				System.out.println("Test passed");
			else
				System.out.println("Test failed at 0");
		}
		if (n==1) {
			if (rs == 1)
			 	System.out.println("Test passed");
			else
				System.out.println("Test failed at 1");
		} 
		if (n==5) {
			if (rs == 120) 
				System.out.println("Test passed");
			else
				System.out.println("Test failed at 5");
		}
	}
	
	public static void main(String args[]) {
		long n;
		n = calculate(0);
		System.out.print("Factorial.calculate(0) returned "+ n + ". "); 
		checkIfPassed(0, n);
		
		n = calculate(1);
		System.out.print("Factorial.calculate(1) returned "+ n + ". ");  
		checkIfPassed(1, n);
		
		n = calculate(5);
		System.out.print("Factorial.calculate(5) returned "+ n + ". "); 
		checkIfPassed(5, n);
	}
}