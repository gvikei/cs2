/**
	Class that represents a polymial (ie, functions of the form a*x^n + b*xn(^−1) + · · · + c*x^2 + dx + e)

	@author Khuyen Duong
*/
public class PolyFunc extends Function {

	int coef[];
    
    public PolyFunc(int[] coefficients) {
		 super();
    	 coef = coefficients;
	}
    
	/**
		Return the power of the highest non-zero term
		
		@return	maxNonZeroTermIndex	the power of the highest non-zero term is also its index
	  */
    public int degree() { // return the index of max value in coefficients[]
        int maxNonZeroTermIndex = 0;
        for (int i = 0; i < coef.length; i++) {
            if (coef[i] > coef[maxNonZeroTermIndex] && coef[i] > 0) {
                maxNonZeroTermIndex = i;
            }
        }
        return maxNonZeroTermIndex;
    }
	 
    /**
		 Return a String representation of the polynomial using x as the variable, 
		 arranged in decreasing order of exponent and printing nothing for terms with a coefficient of zero.
		 For example, the Poly represented by the array [4, 0, −8, 0, 3, 2] should return the string: 
		 2x^5+3x^4-8x^2+4
		 
		 @return		rs		String representation of the polymial
		 */ 
    @Override
    public String toString() {
        String rs = "";
        for (int i = coef.length-1; i >= 1; i--) {
            if (coef[i] != 0) {
                if (i != coef.length-1 && coef[i] > 0) rs += "+";
                rs += coef[i] + "x^" + i;  
            } 
        }
         
        if (coef[0] > 0) rs+= "+";
        rs += coef[0];
        return rs;
    }
    
	 /**
		 Constructs and returns a new Poly object with a new coefficient array - 
		 created by adding the coefficients of the current object and 
		 the Poly object a passed as an argument to the add() function.
		 
		 @param	a 			the other polymial to add to the current one
		 @return rsPoly 	the sum of 2 polymials as a new object 
		 */ 
    public PolyFunc add (PolyFunc a) {
        int maxLen = Math.max (coef.length, a.coef.length);
        int[] rsArray = new int[maxLen];
        for (int i = 0; i < maxLen; i++) {
            if (i < a.coef.length) rsArray[i] += a.coef[i];
            if (i < coef.length) rsArray[i] += coef[i];
        }
        PolyFunc rsPoly = new PolyFunc(rsArray);
        return rsPoly;
    }
    
	 /**
		 Returns the value of the function at the point x
		 
		 @param		x		the value of the point to calculate the function at
		 @return 	rs 	value of the whole function at x
		 */
	 
    @Override
    public double evaluate (double x) {
        double rs = 0;
        for (int i = 0; i < coef.length; i++) {
            rs += coef[i] * Math.pow(x,i);
        }
        return rs;
    }

}
