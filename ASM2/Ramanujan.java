public class Ramanujan {
	public static double piCal (int k) {
		double rs;
		double leftSide = (2*Math.sqrt(2))/9801; 
		double rightSide = 0;
		
		for (int i = 0; i <= k; i++) {
			long fact = Factorial.calculate(i);
			rightSide += ((Factorial.calculate(i*4)) * (1103 + 26390*i))/((Math.pow(fact,4) * Math.pow(396,4*i)));
		}
		
		rs = 1/(rightSide * leftSide);
		return rs;
	}
	
	public static void main(String args[]) {
		
		// checking args
		if (args.length != 1) {
			System.out.println("Usage: Ramanujan [k]");
			System.exit(0);
		}
		
		// processing
		int k = Integer.parseInt(args[0]);
		double pi = piCal(k);
		System.out.println(Double.toString(pi));
		
		// calc. error
		System.out.println("Percentage error: " + (Math.abs(pi-Math.PI)/Math.PI)*100 + "%");
	}
}