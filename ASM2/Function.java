/**
	Provides a method to find the root of any function from it subclasses
	@author Khuyen Duong
*/

public abstract class Function {
	public abstract double evaluate (double x);
	
	/**
		Returns an approximate root in range [a,b] of an equation.
		The result is accepted in an 'epsilon' amount of error
		
		@param	a 	the lower bound of the root's range
		@param	b	the upper bound of the root's range
		@parm		epsilon	the accepted amount of error
		@return	the root in the specified range	
		*/
	
	public double findRoot(double a, double b, double epsilon) {
		
		double x = (a+b)/2;
		if (Math.abs(a-x) < epsilon) return x;
		if (evaluate(x) * evaluate(a) > 0) {
			return findRoot(x,b,epsilon);
		} 
		else
			return findRoot(a,x,epsilon);
	}
	
	public static void main (String args[]) {
		
		// Poly testing
		int[] a = {-2,-1,1};
		PolyFunc poly = new PolyFunc(a);
		double rsPoly = poly.findRoot(0,2,0.000000000000001);
		
		// Sin testing
		SinFunc sin = new SinFunc();
		double rsSin = sin.findRoot(3,4,0.000000000000001);
		
		// Cos testing
		CosFunc cos = new CosFunc();
		double rsCos = cos.findRoot(1,2,0.000000000000001);
		
		System.out.println("f(" + rsPoly + ") = " + poly.evaluate(Math.round(rsPoly)) + "\n sin(" + rsSin + ") = " + sin.evaluate(rsSin) + "\n cos(" + rsCos + ")= " + cos.evaluate(rsCos));
	}
}