import java.io.*;
import java.lang.*;

public class FibTest {
	public static int fibIter(int n) {
		int a = 1; int b = 1; int rs = 0;
		for (int i = 3; i <= n; i++) {
			rs = a+b;
			a = b;
			b = rs;
		}
		
		if (n <= 2) rs = 1;
		return(rs);
	}
	
	public static int fibRecur(int n) {
		if (n <= 2) return 1;
		return (fibRecur(n-1) + fibRecur(n-2));
	}
	
	public static void main(String args[]) {
		
		//-------------------- init --------------------------
		int n;
		boolean DEBUG = true;
		
		//---------------- testing accuracy ------------------
		/**
			fibIter method was refactored from the #1 assignment - 
			which was scored 100/100. That's why I don't hardcode
			real Fibonacci values to test both methods.
			*/
		if (DEBUG) {
			n = 10;
			for (int i = 1; i <= n; i++) {
				int iter = fibIter(i);
				int recur = fibRecur(i);
				System.out.printf("fibIter[%d]: %d  fibRecur[%d]: %d  Result: ", i, iter, i, recur);
				if (iter == recur) 
					System.out.println("True"); 
				else
					System.out.println("False");
			}
		}
		
		//--------------- running time calc. ------------------
			//init
			n = 40;
			long startTime = 0;
			long endTime = 0;
			long timeDiff = 0;
			
			// ------------------------------- first with iterative method ----------------------------
			startTime = System.currentTimeMillis();
			long fibo = fibIter(n);
			endTime = System.currentTimeMillis();
			
			timeDiff = endTime - startTime;
			System.out.printf("Interative method takes: %d miliseconds\n", timeDiff); 
			 
			//-------------------------------- later with recursive method -----------------------------
			startTime = System.currentTimeMillis();
			fibo = fibRecur(n);
			endTime = System.currentTimeMillis();
			
			timeDiff = endTime - startTime;
			System.out.printf("Recursive method takes: %d miliseconds\n", timeDiff); 
	}
}
