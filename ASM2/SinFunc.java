/**
	Returns the trigonometric sine of an angle

	@author	Khuyen Duong
*/
public class SinFunc extends Function {
	/**
		Returns the trigonometric sine of an angle
		
		@param	x	an angle in radians
		@return		the sine of the argument	
		*/
	@Override
	public double evaluate(double x) {
		return (Math.sin(x));
	}

}
