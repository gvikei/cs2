public class Poly {
	int coef[];
	
	public Poly (int[] coefficients) {
		coef = coefficients;
	}
	
	public int degree() { // return the index of max value in coefficients[]
		int maxNonZeroTermIndex = 0;
		for (int i = 0; i < coef.length; i++) {
			if (coef[i] > coef[maxNonZeroTermIndex] && coef[i] > 0) {
				maxNonZeroTermIndex = i;
			}
		}
		return maxNonZeroTermIndex;
	}
	
	@Override
	public String toString() {
		String rs = "";
		for (int i = coef.length-1; i >= 1; i--) {
			if (coef[i] != 0) {
				if (i != coef.length-1 && coef[i] > 0) rs += "+";
				rs += coef[i] + "x^" + i;	
			}  
		}
		
		if (coef[0] > 0) rs+= "+";
		rs += coef[0];
		return rs;
	}
	
	public Poly add (Poly a) {
		int maxLen = Math.max (coef.length, a.coef.length);
		int[] rsArray = new int[maxLen];
		for (int i = 0; i < maxLen; i++) {
			if (i < a.coef.length) rsArray[i] += a.coef[i];
			if (i < coef.length) rsArray[i] += coef[i];
		}
		Poly rsPoly = new Poly(rsArray); 
		return rsPoly;
	}
	
	public double evaluate (double x) {
		double rs = 0;
		for (int i = 0; i < coef.length; i++) {
			rs += coef[i] * Math.pow(x,i);
		}
		return rs;
	}
	
	public static void main(String args[]) {
		int[] a = {-4, 0 , -8, 0, 3, 2};
		int[] b = {0,-2,4,1};
		Poly func = new Poly (a);
		Poly temp = new Poly (b);
		Poly res = func.add(temp);
		System.out.println(res.toString());
		System.out.println(res.evaluate(2.0));
	}
}