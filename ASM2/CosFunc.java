/**
	Returns the trigonometric cosine of an angle
	
	@author		Khuyen Duong
*/
public class CosFunc extends Function {
	/**
		Returns the cosine of the argument.
		
		@param 	x 	the angle in radians
		@return		the cosine of the argument
		*/
	@Override
	public double evaluate(double x) {
		return (Math.cos(x));
	}
}
