import javax.swing.*;
import java.awt.*;

public class MessageFrame extends JFrame {
	public MessageFrame() {
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension screenSize = kit.getScreenSize();
		setSize(screenSize.width/2, screenSize.height/2);
		
		setTitle("Message in a Bottle");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		MessagePanel panel = new MessagePanel();
		add(panel);
		setVisible(true);
	}
}